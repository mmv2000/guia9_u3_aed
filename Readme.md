# Metodos de Búsqueda
Se solicita escriba un programa en C++ que permita el ingreso y la búsqueda de información, definiendo una función Hash que distribuya 
los registros en el arreglo. Si hubiera colisiones, resolverlas aplicando los siguientes métodos: Reasignación Prueba Lineal (L), 
Reasignación Prueba Cuadrática (C), Reasignación Doble Dirección Hash (D) y Encadenamiento (E). El método de resolución de 
colisiones se debe determinar al momento de ejecutar el programa mediante un parámetro de entrada: ./programa {L|C|D|E}, sumado a esto 
por cada ingreso:
Imprimir el contenido del arreglo y la lista enlazada si corresponde y si ocurrió colisión, indicar dónde y cual fue el desplazamiento 
final.
Por cada búsqueda:
Indicar la posición donde se encuentra y si ocurrió colisión, indicar dónde y cual fue el desplazamiento final.

# Como funciona
El código solicita  un parámetro previo por terminal este es el caracter que luego será el parámetros a tener en cuenta por el programa
para desarrollar las líneas de código. El parámetro consiste en ingresar una letra mayuscula {L,C,D o E} que indicará al programa el método
de solución de colisiones que se generen al momento de recorrer el programa. Sin embargo el método que corresponde a la letra E (encadenamiento)
no se encuentra discponible en el código, limitando las opciones a {L, C o D}, prueba lineal, prueba cuafratica y doble dirección hash respectivamente.

# Para ejecutar
Se debe descargar la carpeta con los respectivos archivos .cpp y .h, luego abrir la terminal en la carpeta donde
se almacenaron los archivos. Prontamente se debe ejecutar el comando make en la terminal para la respectiva compilación 
de los archivos y luego ejecutar el comando ./programa && caracter(Letra {L, C, D}) para iniciar el programa y su interacción. 
Ejemplo de ejecución: ./programa L. En este ejemplo se inciará le programa con el método de reasignación de prueba lineal. 

# Construido con 

- sistema operativo (SO): Ubuntu

- lenguaje de programación: C++

- libreria(s): iostream, string.h

- editor de texto: Atom

# Versiones

- Ubuntu 20.04 LTS

- Atom 1.52.0

# Autor

- Martín Muñoz Vera 
