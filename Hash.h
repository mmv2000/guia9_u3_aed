#include <iostream>
using namespace std;

#ifndef HASH_H
#define HASH_H

class Colision{

  private:

  public:
    // Constructor
    Colision();
    // Metodos de Reasignación en inserción y busqueda de números
    void imprimeColision(int *arreglo, int tam);
    void Prueba_Lineal(int pos, int *arreglo, int tam, int numero);
    void Busqueda_Prueba_Lineal(int pos, int *arreglo, int tam, int numero);
    void Prueba_Cuadratica(int pos, int *arreglo, int tam, int numero);
    void Busqueda_Prueba_Cuadratica(int pos, int *arreglo, int tam, int numero);
    void Doble_Direccion_Hash(int pos, int *arreglo, int tam, int numero);
    void Busqueda_Doble_Direccion(int pos, int *arreglo, int tam, int numero);
};
#endif
