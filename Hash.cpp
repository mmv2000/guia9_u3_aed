#include <iostream>
#include "Hash.h"

using namespace std;

Colision::Colision() {}

// Muestra el arreglo en cada proceso
void Colision::imprimeColision(int *arreglo, int tam){
  for(int i = 0; i < tam; i++){
    cout << i << "|" << arreglo[i] << "|" << endl;
  }
}

void Colision::Prueba_Lineal(int pos, int *arreglo, int tam, int numero){
  int newpos;
  int counter;

  // Ve si el número ya se encuentra en el arreglo
  if(arreglo[pos] != -1 && arreglo[pos] == numero) {
    cout << "El " << numero << " ya esta en la posición: " << pos << endl;
  }
  else{
    counter = 0;
    // Avanza en una posicion
    newpos = pos + 1;
    // Ciclo para ver cada posición del arreglo para agregar el numero
    while(arreglo[newpos] != -1 && newpos <= tam && newpos != pos && arreglo[newpos] != numero){
      newpos = newpos + 1;
      if(newpos == tam +1){
        // Inicio del arreglo
        newpos = 0;
      }
      // Evita pasarse del tamaño del arreglo
      counter = counter + 1;
    }
    // Revisión si contador es igual al tamaño
    if(counter = tam) {
      // no hay espacios para agregar más datos
      cout << "No hay espacios disponibles" << endl;
    }
    // Posición en la que es agregado el número luego de desplazarse
    if (arreglo[newpos] == -1) {
      arreglo[newpos] = numero;
      cout << numero << " se movio a la posición: " << newpos << endl;
      imprimeColision(arreglo, tam);
    }
  }
}

// Funcion similar al ingreso pero esta vez la prueba es con la busqueda de números
void Colision::Busqueda_Prueba_Lineal(int pos, int *arreglo, int tam, int numero){
  int newpos;
  // Verifica si el número ya esta en arreglo
  if(arreglo[pos] != -1 && arreglo[pos] == numero){
    cout << "El " << numero << "se encuentra en los posicion: " << pos << endl;
  }
  else{
    // Avanza una posicion
    newpos = pos + 1;
    while (arreglo[newpos] != -1 && newpos <= tam && newpos != pos && arreglo[newpos] != numero){
      newpos = newpos + 1;
      if(newpos == tam + 1){
        newpos = 0;
      }
    }
    // Verifica si el número ingresado esta en el arreglo
    if(arreglo[newpos] == -1 || (newpos == pos)){
      cout << "El " << numero << "no esta en el arreglo" << endl;
      imprimeColision(arreglo, tam);
    }
    // Si esta muestra la posicion
    else{
      cout << "El " << numero << " se encuentra en la posición " << newpos << endl;
      imprimeColision(arreglo, tam);
    }
  }
}

void Colision::Prueba_Cuadratica(int pos, int *arreglo, int tam, int numero) {
  // Variable que aumentara al cuadrado
  int cuadrado;
  int newpos;

  // Verifica si el número ya esta en el arreglo
  if(arreglo[pos] != -1 && arreglo[pos] == numero) {
    cout << "El " << numero << " ya esta en la posición: " << pos << endl;
  }
  else{
    cuadrado = 1;
    // Avanza a nueva posición incrementada al cuadrado
    newpos = (pos + (cuadrado*cuadrado));
    // Verifica si esta vacía la nueva posicion
    while(arreglo[newpos] != -1 && arreglo[pos] != numero){
      // Avanza en una posición
      cuadrado = cuadrado + 1;
      // Nueva posicion
      newpos = (pos + (cuadrado*cuadrado));
      if(newpos > tam){
        cuadrado = 1;
        // Inicio del arreglo
        newpos = 0;
        pos = 0;
      }
    }

    // Posición del número luego de moverse
    if(arreglo[newpos] == -1){
      arreglo[pos] = numero;
      cout << numero << "se movio a la posición" << newpos << endl;
      imprimeColision(arreglo, tam);
    }
  }
}

// Funcion busqueda cuadratica
void Colision::Busqueda_Prueba_Cuadratica(int pos, int *arreglo, int tam, int numero){
  int cuadrado;
  int newpos;

  if(arreglo[pos] != -1 && arreglo[pos] == numero) {
    cout << numero << "En la posición: " << pos << endl;
  }
  else{
    cuadrado = 1;
    newpos = (pos + (cuadrado*cuadrado));
    while(arreglo[newpos] != -1 && arreglo[pos] != numero){
      cuadrado = cuadrado + 1;
      newpos = (pos + (cuadrado*cuadrado));
      if(newpos > tam){
        cuadrado = 1;
        newpos = 0;
        pos = 0;
      }
    }

    if(arreglo[newpos] == -1){
      cout << numero << "No se encuentra" << endl;
      imprimeColision(arreglo, tam);
    }
    else{
      cout << "El " << numero << " se encuentra en la posición " << newpos << endl;
      imprimeColision(arreglo, tam);
    }
  }
}

void Colision::Doble_Direccion_Hash(int pos, int *arreglo, int tam, int numero){
  int newpos;
    // Verifica si el número si se encuentra en el arreglo
    if(arreglo[pos] != -1 && arreglo[pos] == numero){ // Revisa si el número ya esta en el arreglo
        cout << numero << " se encuentra en la posición " << pos << endl;
    }
    else{
        // Se resta uno al tamaño, arreglo parte desde cero
        newpos = ((pos+ 1)%tam - 1) + 1;
        while(newpos <= tam && arreglo[newpos] != -1 && newpos != pos && arreglo[newpos] != numero){
          newpos = ((newpos + 1)%tam - 1) + 1;
        }
        // Verifica si la nueva posicion se encuentra vacía
        if(arreglo[newpos] == -1){
            arreglo[newpos] = numero;
            if(newpos == tam + 1){
              cout << "No quedan espacios en el arreglo" << endl;
            }
            else{
                cout << numero << " se movio a la posición " << newpos << "\n" << endl;
                imprimeColision(arreglo, tam);
            }
        }
    }
}

void Colision::Busqueda_Doble_Direccion(int pos, int *arreglo, int tam, int numero){
  int newpos;

    if(arreglo[pos] != -1 && arreglo[pos] == numero){ // Revisa si el número ya esta en el arreglo
        cout << "El " << numero << " se encuentra en la posición " << pos << endl;
    }
    else{
        newpos = ((pos + 1)%tam - 1) + 1;
        while(newpos <= tam && arreglo[newpos] != -1 && newpos != pos && arreglo[newpos] != numero){
          newpos = ((newpos + 1)%tam -1) + 1; // Se resta uno al tamaño porque el arreglo parte desde cero
        }

        if(arreglo[newpos] == -1 || (newpos == pos)){
            cout << numero << " no se encuentra " << endl;
            imprimeColision(arreglo, tam);
        }

	    else{
            cout << "El " << numero << " se encuentra en la posición " << newpos << endl;
            imprimeColision(arreglo, tam);
        }
    }
}
