#include <iostream>
#include <string.h>

#include "Hash.h"

using namespace std;

// Rellena arreglo con -1
void creaArreglo(int *arreglo, int tam) {
  for(int i = 0; i < tam; i++){
    arreglo[i] = -1;
  }
  cout << "\n" << endl;
}

// Imprime el arreglo
void Imprime(int *arreglo, int tam) {
  for(int i = 0; i < tam; i++){
    cout << i << "|" << arreglo[i] << "|" << endl;
  }
}

// Colisiones en el ingreso de datos
void Colision_insercion(int *arreglo, int tam, int numero, char *argv[], int pos) {
  Colision colision = Colision();
  string metodo = argv[1];

  if(metodo == "L"){
    colision.Prueba_Lineal(pos, arreglo, tam, numero);
  }
  else if(metodo == "C"){
    colision.Prueba_Cuadratica(pos, arreglo, tam, numero);
  }
  else if(metodo == "D"){
    colision.Doble_Direccion_Hash(pos, arreglo, tam, numero);
  }
}

// Colisiones en la busqueda de datos
void Colision_Busqueda(int *arreglo, int tam, int numero, char *argv[], int pos){
  Colision colision = Colision();
  string metodo = argv[1];

  if(metodo == "L"){
    colision.Busqueda_Prueba_Lineal(pos, arreglo, tam, numero);
  }
  else if(metodo == "C"){
    colision.Busqueda_Prueba_Cuadratica(pos, arreglo, tam, numero);
  }
}

// Funcion hash por módulo
void Hash(int *arreglo, int tam, int numero, char *argv[]) {
  int pos;

  pos = (numero%(tam -1)) + 1;

  if(arreglo[pos] != -1) {
    cout << "Colisión en posición: " << pos << endl;
    Colision_insercion(arreglo, tam, numero, argv, pos);
  }
  else{
    arreglo[pos] = numero;
    Imprime(arreglo, tam);
  }
}

// Valida que el arreglo no se haya llenado
bool Verificar_arreglo(int *arreglo, int tam){
  int cont;

  for(int i = 0; i < tam; i++){
    if(arreglo[i] != -1){
      cont++;
    }
  }
  if(cont <= tam && cont > 0){
    return false;
  }
  else{
    return true;
  }
}

// Retorna numero ingresado por el usuario
int Ingresar(){
  int numero;
  cin >> numero;
  return numero;
}

// Menu programa
int menu(char *argv[], int tam) {
  int numero;
  int arreglo[tam];
  int op;
  int pos;
  bool flag;
  /* Crea arreglo rellenandolo con valor -1,
  indicando que no hay datos en esa posición*/
  creaArreglo(arreglo, tam);

  do {
    cout << "Ingrese una opción:" << endl;
    cout << "1. Ingresar números" << endl;
    cout << "2. Buscar un número" << endl;
    cout << "3. Salir" << endl;

    cout << "Ingrese su opcion" << endl;
    cin >> op;

    switch (op) {
      // Ingresa numeros verificando posibles colisiones al haber valores iguales
      case 1:
        system("clear");
        cout << "Ingrese número" << endl;
        numero = Ingresar();
        // Funcion hash por modulo o división
        Hash(arreglo, tam, numero, argv);
        break;

      case 2:
        system("clear");
        // Dependiendo del bool retornado, busca número en el arreglo
        flag = Verificar_arreglo(arreglo, tam);
        if(flag == true){
          cout << "Ingrese número a buscar" << endl;
          numero = Ingresar();
          pos = (numero%(tam - 1)) + 1;

          /* Si el numero ingresado estaba previamente, muestra la posicion
          en la que se encontro*/
          if(arreglo[pos] == numero){
            cout << numero << " Encontrado en la posicion " << pos << endl;
            Imprime(arreglo, tam);
          }
          else{
            // Si no es encontrado muestra colisión en esa posición
            cout << "Colision en posicion: " << pos << endl;
            Colision_Busqueda(arreglo, tam, numero, argv, pos);
          }

        }
        else{
          cout << "Arreglo vacio" << endl;
          break;
        }

    }
  } while(op != 3);
}

int Parametros(int argc, char *argv[], int tam) {
  // Valida cantidad de parametros ingresados
  if(argc == 2){
    menu(argv, tam);
  }
  else{
    cout << "No ha ingresado suficientes parámetros" << endl;
    cout << "Vuelva a reiniciar el programa" << endl;
    return -1;
  }
}

// Main
int main(int argc, char *argv[]) {
  int entrada;
  int tam;

  cout << "Empezando ingrese tamaño del arreglo: " << endl;
  cin >> tam;
  entrada = Parametros(argc, argv, tam);
  return 0;
}
